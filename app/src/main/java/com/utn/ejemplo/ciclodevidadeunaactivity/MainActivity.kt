package com.utn.ejemplo.ciclodevidadeunaactivity

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter

class MainActivity : AppCompatActivity() {
    companion object {
        private const val ETIQUETA = "CURSO"
        private const val LOGS = "logs"
        private const val LOG_INICIAL = "Inicio de los logs"
    }


    private lateinit var btnFinish: View
    private lateinit var tvLogs: TextView

    private var logs: String = LOG_INICIAL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        logs = savedInstanceState?.getString(LOGS) ?: LOG_INICIAL

        btnFinish = findViewById(R.id.finish)
        btnFinish.setOnClickListener {
            finish()    // Causa el cierre y destrucción de la Activity. No es parte del ciclo de vida
        }

        tvLogs = findViewById(R.id.logs)
        tvLogs.text = logs

        log("onCreate: Activity creada. También se puede restaurar el Bundle de estado")
    }

    override fun onStart() {
        super.onStart()

        log("onStart: Activity visible (no en primer plano)")
    }

    override fun onResume() {
        super.onResume()

        log("onResume: Activity en primer plano. Interacción directa con el usuario")
    }

    override fun onPause() {
        super.onPause()

        log("onPause: Activity perdió el primer plano, pero puede estar visible")
    }

    override fun onStop() {
        super.onStop()

        log("onStop: Activity dejó de estar visible")
    }

    override fun onDestroy() {
        super.onDestroy()

        log("onDestroy: Activity será destruída y no se podrá volver a utilizar")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        log("onSaveInstanceState: Guardando el estado antes de la destrucción de la Activity")

        outState.putString(LOGS, logs)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        log("onRestoreInstanceState: Restaurando estado de Activity que fue destruída")
    }

    override fun onRestart() {
        super.onRestart()

        log("onRestart: Activity a punto de ser mostrada luego de estar invisible (es decir, luego de habe pasado por onStop)")
    }

    private fun log(texto: String) {
        Log.d(ETIQUETA, texto)

        val now = System.currentTimeMillis()
        val hora = SimpleDateFormat.getTimeInstance().format(now)

        logs = "$logs\n\n${hora}: $texto"
        tvLogs.text = logs
    }


}